# example-nestjs

> nestjs example

## Version Info

| technology | version |
| ---------- | ------- |
| apollo     | 2       |
| graphql    | 15      |
